﻿namespace prvi_zadatak
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plus_gumb = new System.Windows.Forms.Button();
            this.puta_gumb = new System.Windows.Forms.Button();
            this.sin_gumb = new System.Windows.Forms.Button();
            this.broj_1 = new System.Windows.Forms.TextBox();
            this.broj_2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rezultat_text = new System.Windows.Forms.Label();
            this.cos_gumb = new System.Windows.Forms.Button();
            this.dijeli_gumb = new System.Windows.Forms.Button();
            this.minus_gumb = new System.Windows.Forms.Button();
            this.log_gumb = new System.Windows.Forms.Button();
            this.kvadrat_gumb = new System.Windows.Forms.Button();
            this.sqrt_gumb = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // plus_gumb
            // 
            this.plus_gumb.Location = new System.Drawing.Point(38, 132);
            this.plus_gumb.Name = "plus_gumb";
            this.plus_gumb.Size = new System.Drawing.Size(72, 41);
            this.plus_gumb.TabIndex = 2;
            this.plus_gumb.Text = "+";
            this.plus_gumb.UseVisualStyleBackColor = true;
            this.plus_gumb.Click += new System.EventHandler(this.plus_gumb_Click);
            // 
            // puta_gumb
            // 
            this.puta_gumb.Location = new System.Drawing.Point(38, 215);
            this.puta_gumb.Name = "puta_gumb";
            this.puta_gumb.Size = new System.Drawing.Size(72, 41);
            this.puta_gumb.TabIndex = 3;
            this.puta_gumb.Text = "*";
            this.puta_gumb.UseVisualStyleBackColor = true;
            this.puta_gumb.Click += new System.EventHandler(this.puta_gumb_Click);
            // 
            // sin_gumb
            // 
            this.sin_gumb.Location = new System.Drawing.Point(38, 297);
            this.sin_gumb.Name = "sin_gumb";
            this.sin_gumb.Size = new System.Drawing.Size(72, 41);
            this.sin_gumb.TabIndex = 4;
            this.sin_gumb.Text = "sin";
            this.sin_gumb.UseVisualStyleBackColor = true;
            this.sin_gumb.Click += new System.EventHandler(this.sin_gumb_Click);
            // 
            // broj_1
            // 
            this.broj_1.Location = new System.Drawing.Point(12, 42);
            this.broj_1.Name = "broj_1";
            this.broj_1.Size = new System.Drawing.Size(100, 22);
            this.broj_1.TabIndex = 11;
            // 
            // broj_2
            // 
            this.broj_2.Location = new System.Drawing.Point(257, 42);
            this.broj_2.Name = "broj_2";
            this.broj_2.Size = new System.Drawing.Size(100, 22);
            this.broj_2.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Prvi Broj";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Drugi Broj";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(159, 372);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "Rezultat :";
            // 
            // rezultat_text
            // 
            this.rezultat_text.AutoSize = true;
            this.rezultat_text.Location = new System.Drawing.Point(182, 401);
            this.rezultat_text.Name = "rezultat_text";
            this.rezultat_text.Size = new System.Drawing.Size(16, 17);
            this.rezultat_text.TabIndex = 16;
            this.rezultat_text.Text = "0";
            // 
            // cos_gumb
            // 
            this.cos_gumb.Location = new System.Drawing.Point(162, 297);
            this.cos_gumb.Name = "cos_gumb";
            this.cos_gumb.Size = new System.Drawing.Size(72, 41);
            this.cos_gumb.TabIndex = 19;
            this.cos_gumb.Text = "cos";
            this.cos_gumb.UseVisualStyleBackColor = true;
            this.cos_gumb.Click += new System.EventHandler(this.cos_gumb_Click);
            // 
            // dijeli_gumb
            // 
            this.dijeli_gumb.Location = new System.Drawing.Point(162, 215);
            this.dijeli_gumb.Name = "dijeli_gumb";
            this.dijeli_gumb.Size = new System.Drawing.Size(72, 41);
            this.dijeli_gumb.TabIndex = 18;
            this.dijeli_gumb.Text = "/";
            this.dijeli_gumb.UseVisualStyleBackColor = true;
            this.dijeli_gumb.Click += new System.EventHandler(this.dijeli_gumb_Click);
            // 
            // minus_gumb
            // 
            this.minus_gumb.Location = new System.Drawing.Point(162, 132);
            this.minus_gumb.Name = "minus_gumb";
            this.minus_gumb.Size = new System.Drawing.Size(72, 41);
            this.minus_gumb.TabIndex = 17;
            this.minus_gumb.Text = "-";
            this.minus_gumb.UseVisualStyleBackColor = true;
            this.minus_gumb.Click += new System.EventHandler(this.minus_gumb_Click);
            // 
            // log_gumb
            // 
            this.log_gumb.Location = new System.Drawing.Point(285, 297);
            this.log_gumb.Name = "log_gumb";
            this.log_gumb.Size = new System.Drawing.Size(72, 41);
            this.log_gumb.TabIndex = 22;
            this.log_gumb.Text = "log";
            this.log_gumb.UseVisualStyleBackColor = true;
            this.log_gumb.Click += new System.EventHandler(this.log_gumb_Click);
            // 
            // kvadrat_gumb
            // 
            this.kvadrat_gumb.Location = new System.Drawing.Point(282, 215);
            this.kvadrat_gumb.Name = "kvadrat_gumb";
            this.kvadrat_gumb.Size = new System.Drawing.Size(72, 41);
            this.kvadrat_gumb.TabIndex = 21;
            this.kvadrat_gumb.Text = "x^2";
            this.kvadrat_gumb.UseVisualStyleBackColor = true;
            this.kvadrat_gumb.Click += new System.EventHandler(this.kvadrat_gumb_Click);
            // 
            // sqrt_gumb
            // 
            this.sqrt_gumb.Location = new System.Drawing.Point(278, 132);
            this.sqrt_gumb.Name = "sqrt_gumb";
            this.sqrt_gumb.Size = new System.Drawing.Size(72, 41);
            this.sqrt_gumb.TabIndex = 20;
            this.sqrt_gumb.Text = "sqrt";
            this.sqrt_gumb.UseVisualStyleBackColor = true;
            this.sqrt_gumb.Click += new System.EventHandler(this.sqrt_gumb_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 450);
            this.Controls.Add(this.log_gumb);
            this.Controls.Add(this.kvadrat_gumb);
            this.Controls.Add(this.sqrt_gumb);
            this.Controls.Add(this.cos_gumb);
            this.Controls.Add(this.dijeli_gumb);
            this.Controls.Add(this.minus_gumb);
            this.Controls.Add(this.rezultat_text);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.broj_2);
            this.Controls.Add(this.broj_1);
            this.Controls.Add(this.sin_gumb);
            this.Controls.Add(this.puta_gumb);
            this.Controls.Add(this.plus_gumb);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button plus_gumb;
        private System.Windows.Forms.Button puta_gumb;
        private System.Windows.Forms.Button sin_gumb;
        private System.Windows.Forms.TextBox broj_1;
        private System.Windows.Forms.TextBox broj_2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label rezultat_text;
        private System.Windows.Forms.Button cos_gumb;
        private System.Windows.Forms.Button dijeli_gumb;
        private System.Windows.Forms.Button minus_gumb;
        private System.Windows.Forms.Button log_gumb;
        private System.Windows.Forms.Button kvadrat_gumb;
        private System.Windows.Forms.Button sqrt_gumb;
    }
}

