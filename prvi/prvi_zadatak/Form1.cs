﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prvi_zadatak
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void plus_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1))
                MessageBox.Show("Pogresan unos broj 1!", "Pogreska!");
            else if (!double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj 2!", "Pogreska!");
            else
            {
                rezultat_text.Text = (broj1 + broj2).ToString();
            }
        }

        private void minus_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1))
                MessageBox.Show("Pogresan unos broj 1!", "Pogreska!");
            else if (!double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj 2!", "Pogreska!");
            else
            {
                rezultat_text.Text = (broj1 - broj2).ToString();
            }
        }

        private void sqrt_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1) && !double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj!", "Pogreska!");
            else if (double.TryParse(broj_2.Text, out broj2) && double.TryParse(broj_1.Text, out broj1))
            {
                MessageBox.Show("Unijeti samo jedan broj!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(broj_2.Text, out broj2) || double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Sqrt(broj1)).ToString();
            }
            else if (double.TryParse(broj_2.Text, out broj2) || !double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Sqrt(broj2)).ToString();
            }
        }

        private void puta_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1))
                MessageBox.Show("Pogresan unos broj 1!", "Pogreska!");
            else if (!double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj 2!", "Pogreska!");
            else
            {
                rezultat_text.Text = (broj1 * broj2).ToString();
            }
        }

        private void dijeli_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1))
                MessageBox.Show("Pogresan unos broj 1!", "Pogreska!");
            else if (!double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj 2!", "Pogreska!");
            else
            {
                rezultat_text.Text = (broj1 / broj2).ToString();
            }
        }

        private void kvadrat_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1) && !double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj!", "Pogreska!");
            else if (double.TryParse(broj_2.Text, out broj2) && double.TryParse(broj_1.Text, out broj1))
            {
                MessageBox.Show("Unijeti samo jedan broj!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(broj_2.Text, out broj2) || double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Pow((broj1), 2)).ToString();
            }
            else if (double.TryParse(broj_2.Text, out broj2) || !double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Pow((broj2), 2)).ToString();
            }
        }

        private void sin_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1) && !double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj!", "Pogreska!");
            else if (double.TryParse(broj_2.Text, out broj2) && double.TryParse(broj_1.Text, out broj1))
            {
                MessageBox.Show("Unijeti samo jedan broj!");
            } //dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(broj_2.Text, out broj2) || double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Sin(broj1)).ToString();
            }
            else if (double.TryParse(broj_2.Text, out broj2) || !double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Sin(broj2)).ToString();
            }
        }

        private void cos_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1) && !double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos broj!", "Pogreska!");
            else if (double.TryParse(broj_2.Text, out broj2) && double.TryParse(broj_1.Text, out broj1))
            {
                MessageBox.Show("Unijeti samo jedan broj!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(broj_2.Text, out broj2) || double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Cos(broj1)).ToString();
            }
            else if (double.TryParse(broj_2.Text, out broj2) || !double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Cos(broj2)).ToString();
            }
        }

        private void log_gumb_Click(object sender, EventArgs e)
        {
            double broj1, broj2;
            if (!double.TryParse(broj_1.Text, out broj1) && !double.TryParse(broj_2.Text, out broj2))
                MessageBox.Show("Pogresan unos brih!", "Pogreska!");
            else if (double.TryParse(broj_2.Text, out broj2) && double.TryParse(broj_1.Text, out broj1))
            {
                MessageBox.Show("Unijeti samo jedan broj!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(broj_2.Text, out broj2) || double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Log10(broj1)).ToString();
            }
            else if (double.TryParse(broj_2.Text, out broj2) || !double.TryParse(broj_1.Text, out broj1))
            {
                rezultat_text.Text = (Math.Log10(broj2)).ToString();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}