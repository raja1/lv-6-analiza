﻿namespace drugi_zadatak
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.broj_pokusaja = new System.Windows.Forms.Label();
            this.popis_text = new System.Windows.Forms.ListBox();
            this.bubaj_slovo = new System.Windows.Forms.Button();
            this.bubaj_rijec = new System.Windows.Forms.Button();
            this.pogodi_bubaj = new System.Windows.Forms.Button();
            this.slovo_text = new System.Windows.Forms.TextBox();
            this.rijec_text = new System.Windows.Forms.TextBox();
            this.pokreni = new System.Windows.Forms.Button();
            this.ploca = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Broj pokusaja";
            // 
            // broj_pokusaja
            // 
            this.broj_pokusaja.AutoSize = true;
            this.broj_pokusaja.Location = new System.Drawing.Point(146, 21);
            this.broj_pokusaja.Name = "broj_pokusaja";
            this.broj_pokusaja.Size = new System.Drawing.Size(16, 17);
            this.broj_pokusaja.TabIndex = 1;
            this.broj_pokusaja.Text = "5";
            // 
            // popis_text
            // 
            this.popis_text.FormattingEnabled = true;
            this.popis_text.ItemHeight = 16;
            this.popis_text.Location = new System.Drawing.Point(77, 154);
            this.popis_text.Name = "popis_text";
            this.popis_text.Size = new System.Drawing.Size(120, 84);
            this.popis_text.TabIndex = 2;
            // 
            // bubaj_slovo
            // 
            this.bubaj_slovo.Location = new System.Drawing.Point(18, 87);
            this.bubaj_slovo.Name = "bubaj_slovo";
            this.bubaj_slovo.Size = new System.Drawing.Size(75, 23);
            this.bubaj_slovo.TabIndex = 3;
            this.bubaj_slovo.Text = "Slovo";
            this.bubaj_slovo.UseVisualStyleBackColor = true;
            this.bubaj_slovo.Click += new System.EventHandler(this.bubaj_slovo_Click);
            // 
            // bubaj_rijec
            // 
            this.bubaj_rijec.Location = new System.Drawing.Point(181, 87);
            this.bubaj_rijec.Name = "bubaj_rijec";
            this.bubaj_rijec.Size = new System.Drawing.Size(75, 23);
            this.bubaj_rijec.TabIndex = 4;
            this.bubaj_rijec.Text = "Rijec";
            this.bubaj_rijec.UseVisualStyleBackColor = true;
            // 
            // pogodi_bubaj
            // 
            this.pogodi_bubaj.Location = new System.Drawing.Point(100, 244);
            this.pogodi_bubaj.Name = "pogodi_bubaj";
            this.pogodi_bubaj.Size = new System.Drawing.Size(75, 42);
            this.pogodi_bubaj.TabIndex = 5;
            this.pogodi_bubaj.Text = "Pogađaj";
            this.pogodi_bubaj.UseVisualStyleBackColor = true;
            // 
            // slovo_text
            // 
            this.slovo_text.Location = new System.Drawing.Point(18, 116);
            this.slovo_text.Name = "slovo_text";
            this.slovo_text.Size = new System.Drawing.Size(100, 22);
            this.slovo_text.TabIndex = 6;
            // 
            // rijec_text
            // 
            this.rijec_text.Location = new System.Drawing.Point(156, 116);
            this.rijec_text.Name = "rijec_text";
            this.rijec_text.Size = new System.Drawing.Size(100, 22);
            this.rijec_text.TabIndex = 7;
            // 
            // pokreni
            // 
            this.pokreni.Location = new System.Drawing.Point(208, 21);
            this.pokreni.Name = "pokreni";
            this.pokreni.Size = new System.Drawing.Size(75, 23);
            this.pokreni.TabIndex = 8;
            this.pokreni.Text = "START";
            this.pokreni.UseVisualStyleBackColor = true;
            this.pokreni.Click += new System.EventHandler(this.pokreni_Click);
            // 
            // ploca
            // 
            this.ploca.AutoSize = true;
            this.ploca.Location = new System.Drawing.Point(35, 64);
            this.ploca.Name = "ploca";
            this.ploca.Size = new System.Drawing.Size(13, 17);
            this.ploca.TabIndex = 9;
            this.ploca.Text = "*";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 328);
            this.Controls.Add(this.ploca);
            this.Controls.Add(this.pokreni);
            this.Controls.Add(this.rijec_text);
            this.Controls.Add(this.slovo_text);
            this.Controls.Add(this.pogodi_bubaj);
            this.Controls.Add(this.bubaj_rijec);
            this.Controls.Add(this.bubaj_slovo);
            this.Controls.Add(this.popis_text);
            this.Controls.Add(this.broj_pokusaja);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Vješala";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label broj_pokusaja;
        private System.Windows.Forms.ListBox popis_text;
        private System.Windows.Forms.Button bubaj_slovo;
        private System.Windows.Forms.Button bubaj_rijec;
        private System.Windows.Forms.Button pogodi_bubaj;
        private System.Windows.Forms.TextBox slovo_text;
        private System.Windows.Forms.TextBox rijec_text;
        private System.Windows.Forms.Button pokreni;
        private System.Windows.Forms.Label ploca;
    }
}

