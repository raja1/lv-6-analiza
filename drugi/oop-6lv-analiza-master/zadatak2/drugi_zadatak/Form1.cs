﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace drugi_zadatak
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        class Rijec
        {
            #region data_members
            private string ime;

            #endregion
            #region public_methods
            public Rijec()
            {
                ime = "word";
            }
            public Rijec(string n)
            {
                ime = n;
            }
            public override string ToString()
            {
                return ime;
            }
            #endregion
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {
                    string[] parts = line.Split('\t');
                    Rijec W = new Rijec(parts[0]); // novi objekt
                    listwords.Add(W); // umetanje objekta u listu
                }
                popis_text.DataSource = null;
                popis_text.DataSource = listwords;
            }

        }

        List<Rijec> listwords = new List<Rijec>();
        string path = "C:\\data.txt";
        int m;

        private void bubaj_slovo_Click(object sender, EventArgs e)
        {
            //gumb za provjeru da li rijec sadrzi upisano slovo
            //smanjivanje zivota ukoliko ne sadrzi
            int broj, temp;
            string b = slovo_text.Text;
            string a = popis_text.Items[m].ToString();
            if (slovo_text.Text == "")
                MessageBox.Show("Unesi nesto", "error");
            else
            {
                if (a.Contains(b))
                    ploca.Text = "Sadrzi to slovo!";
                else
                {
                    broj = Int32.Parse(broj_pokusaja.Text);
                    if (broj <= 0)
                    {
                        MessageBox.Show("Loš kraj");
                        broj_pokusaja.Text = "0";

                    }
                    temp = broj - 1;


                    broj_pokusaja.Text = temp.ToString();
                    if (temp <= 0 || temp == -1)
                    {
                        MessageBox.Show("Not so good for you");
                        broj_pokusaja.Text = "0";
                    }

                }
            }



        }

        private void pokreni_Click(object sender, EventArgs e)
        {
            //nasumican odabir rijeci iz liste
            Random random = new Random();
            int a = random.Next(0, popis_text.Items.Count);
            string b = popis_text.Items[a].ToString();
            int c = b.Length - 1;
            string d = "_ ";
            string result = string.Concat(Enumerable.Repeat(d, c));
            ploca.Text = b.ElementAt(0).ToString() + result;
            m = a;
            broj_pokusaja.Text = "5";
        }
    }
}
